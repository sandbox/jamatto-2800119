
-- SUMMARY --

Used by thousands of bloggers around the world. Accept Jamatto micropayments 
with a single click for your premium content or for donations on your blog.

For a full description of the module, visit the project page:
  http://drupal.org/project/jamatto_micropayments

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/jamatto_micropayments

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install this module as per any other Drupal module.  See 
  http://drupal.org/node/895232 for further information.
* Go to Home >> Administration >> Modules
* Tick the jamatto_micropayments module to enable it (it is probably at the 
  bottom of the screen).
* Press the 'Save configuration' button.
* Press the 'Configure' hyperlink next to the jamatto_micropayments module.
* That should have taken you to 
  Home >> Administration >> Configuration >> Content Authoring
* Click the 'Text formats' link.
* Click the 'configure' hyperlink next to the 'Plain text' item.
* In the 'Enabled filters' section, tick the Jamatto Micropayments module.
* Scroll to the bottom and press 'Save configuration'
* In the 'Filter processing order' section, make sure that Jamatto 
  Micropayments comes after 'Display any HTML as plain text'
* Scroll to the bottom and press 'Save configuration'

-- CONFIGURATION --

* Go to Home >> Administration >> Configuration >> Content Authoring
* In the 'Filter settings' section, click the 'Jamatto Micropayments' tab
* Override any settings you like there.  The most important is to get your own
  Business ID (BID) from the Jamatto.com website and fill it in there.

-- FAQ --

Q: What does it cost to use Jamatto buttons?

A: It is free to install Jamatto buttons on your website.  A percentage of any
   revenues you receive through your buttons will be kept by Jamatto.  Full 
   terms and conditions are available at https://jamatto.com

-- CONTACT --

Current maintainers:
* Jamatto (jamatto) - https://www.drupal.org/u/jamatto

For support:
* support@jamatto.com

This project has been sponsored by:
* Jamatto.com - visit https://jamatto.com
